/**
 * 
 */
package de.tobidodo.spritmonclient.exception;

/**
 * @author Reeny
 *
 */
public class CarIdNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 6631655308187217581L;

	public CarIdNotFoundException() {
		super();
	}

	public CarIdNotFoundException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public CarIdNotFoundException(String detailMessage) {
		super(detailMessage);
	}

	public CarIdNotFoundException(Throwable throwable) {
		super(throwable);
	}

}
