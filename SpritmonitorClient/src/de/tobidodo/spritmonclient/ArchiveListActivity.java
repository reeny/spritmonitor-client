/*
 * Copyright (C) 2011 Reeny
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tobidodo.spritmonclient;

import android.app.ListActivity;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.SimpleCursorAdapter;
import de.tobidodo.spritmonclient.data.FuelDataProvider;

/**
 * @author Reeny
 *
 */
public class ArchiveListActivity extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//String[] selectCols = new String[]{FuelDataProvider._ID, FuelDataProvider.FUEL_DATE, FuelDataProvider.FUEL_IN, FuelDataProvider.FUEL_COST, FuelDataProvider.TRANSMIT_TIME};
		Cursor fuelArchiveDataCursor = this.getContentResolver().query(FuelDataProvider.CONTENT_LIST_URI, null, null, null, FuelDataProvider.FUEL_DATE);
				/*
				managedQuery(
				FuelDataProvider.CONTENT_LIST_URI,
				selectCols,
				null,
				null,
				FuelDataProvider.FUEL_DATE);
				*/
		
		String[] selectCols = new String[]{FuelDataProvider.FUEL_DATE, FuelDataProvider.FUEL_IN, FuelDataProvider.FUEL_COST, FuelDataProvider.TRANSMIT_TIME};
		int[] viewIDs = { R.id.item_text_1, R.id.item_text_2, R.id.item_text_3, R.id.item_text_10 };
		
        // Creates the backing adapter for the ListView.
        SimpleCursorAdapter adapter
            = new SimpleCursorAdapter(
                      this,                             // The Context for the ListView
                      R.layout.item_archive_list,       // Points to the XML for a list item
                      fuelArchiveDataCursor,            // The cursor to get items from
                      selectCols,
                      viewIDs
              );

		setListAdapter(adapter);

	}

}
