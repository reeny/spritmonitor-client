/*
 * Copyright (C) 2011 Reeny
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tobidodo.spritmonclient.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class SaveErrorMsgOfFuelData extends Service{

	public static final String ATTR_DB_ID = "fueldata_dbid";
	public static final String ATTR_ERR_ID = "fueldata_error_id";

	
	
	@Override
	public IBinder onBind(final Intent intent) {
		return null;
	}
	

	@Override
	public void onCreate() {
		super.onCreate();
		
	}
	
	@Override
	public void onDestroy() {
		
		super.onDestroy();
	}
	
	@Override
	public int onStartCommand(final Intent intent, final int flags, final int startId) {

		if (intent.hasExtra(ATTR_DB_ID) && intent.hasExtra(ATTR_ERR_ID)){
			final long dataToUpdateId = intent.getLongExtra(ATTR_DB_ID, -1L);
			final int dataErrorId = intent.getIntExtra(ATTR_ERR_ID, -1);
			
			if (dataToUpdateId != -1L){
				
				Thread t = new Thread(new Runnable() {
					
					@Override
					public void run() {
						stopSelf(startId);
					}
				});
				
				t.start();
			}
		}
		
		
		return START_NOT_STICKY;
	}
	

}
