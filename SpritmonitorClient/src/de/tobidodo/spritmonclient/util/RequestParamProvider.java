/*
 * Copyright (C) 2011 Reeny
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tobidodo.spritmonclient.util;

public interface RequestParamProvider {

	public String getLoginUserName();
	public String getLoginUserPassword();
	public String getTotalDistanceValue();
	public String getPeriodDistanceFormValue();
	public String getFuelAmountFormValue();
	public String getFuelPriceFormValue();
	public String getFuelType();
}
