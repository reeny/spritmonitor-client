/*
 * Copyright (C) 2011 Reeny
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tobidodo.spritmonclient.util;

import android.app.AlertDialog;
import android.content.Context;

/**
 * A utility class for creating dialogs.
 * @author user
 *
 */
public class DialogHelper {

	/**
	 * Create an error dialog that shows a error message which
	 * is given by a reference id. The dialog has the title
	 * "error" and is cancable.
	 * @param parent The context which is given as parent of the
	 * 		created dialog. Must not be <code>null</code>.
	 * @param msgRefId The id of the message which references the
	 * 		message text in the resources.
	 * @return The created error dialog.
	 * @see AlertDialog.Builder
	 */
	public static AlertDialog createErrorDialog(final Context parent, final int msgRefId) {
		AlertDialog.Builder builder = new AlertDialog.Builder(parent);
		builder.setTitle("Error").setMessage(msgRefId).setCancelable(true);
		
		return builder.create();
	}
	

	/**
	 * Create an error dialog that shows a error message text.
	 * The dialog has the title "error" and is cancable.
	 * @param parent The context which is given as parent of the
	 * 		created dialog. Must not be <code>null</code>.
	 * @param message The text of the error message.
	 * @return The created error dialog.
	 * @see AlertDialog.Builder
	 */
	public static AlertDialog createErrorDialog(final Context parent, final String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(parent);
		builder.setTitle("Error").setMessage(message).setCancelable(true);
		
		return builder.create();
	}
	


}
