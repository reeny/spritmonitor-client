/*
 * Copyright (C) 2011 Reeny
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tobidodo.spritmonclient.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;

import android.util.Log;

/**
 * Delegate to the android log system.
 * @author user
 *
 */
public final class LogHelper {
	
	private static final String LOGNAME = "SpritMonitorClient";
	
	public static void logRequestResult(final HttpResponse response) {
		ByteArrayOutputStream outstream = new ByteArrayOutputStream();
		try {
			response.getEntity().writeTo(outstream);
		} catch (IOException e) {
			logErrorVerbose("Fehler: "+e, e);
		}
		
		logInfo("response: "+outstream.toString());
	}
	
	/**
	 * Logs a text to the android log output with an INFO level.
	 * @param text The message to log.
	 */
	public static void logInfo(final String text){
		Log.i(LOGNAME, text);
	}

	/**
	 * Logs an error to the android log output.
	 * @param errortext The error message to log.
	 */
	public static void logError(final String errortext){
		Log.e(LOGNAME, errortext);
	}
	
	/**
	 * Logs an error to the android log output.
	 * @param errortext The error message to log.
	 * @param error The error with a stacktrace.
	 */
	public static void logError(final String errortext, final Throwable error){
		Log.e(LOGNAME, errortext, error);
	}
	
	/**
	 * First logs an error to android log and then logs the error
	 * message with a stacktrace to the standard error output.
	 * @param errortext The error message to log.
	 * @param error The error with a stacktrace.
	 */
	public static void logErrorVerbose(final String errortext, final Throwable error){
		logError(errortext, error);
		
		System.err.println("Error: "+error);
		error.printStackTrace();
	}
}
