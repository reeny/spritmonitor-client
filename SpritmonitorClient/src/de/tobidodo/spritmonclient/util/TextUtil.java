/*
 * Copyright (C) 2011 Reeny
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tobidodo.spritmonclient.util;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A utility class for different text purposes.
 * @author user
 */
public class TextUtil implements Serializable {

	private static final long serialVersionUID = 343627764000942901L;

	/**
	 * Return the current date and time as string with the
	 * given format.
	 * @param formatString The format for the resulting date string.
	 * @return The current time with the given format.
	 */
	public static String getCurrentDate(final String formatString){
		return new SimpleDateFormat(formatString).format(new Date());
	}
	
	/**
	 * Return <code>true</code> if the given text is not
	 * <code>null</code> or if the length is zero.
	 * @param text The text to check.
	 * @return <code>true</code> if the text is empty.
	 */
	public static boolean isEmpty(final String text){
		return text == null || text.length() == 0;
	}
	
	/**
	 * Return <code>true</code> if the given text is
	 * not <code>null</code> and has at least one char.
	 * @param text The text to check.
	 * @return <code>true</code> if the text is not empty.
	 */
	public static boolean isNotEmpty(final String text){
		return !isEmpty(text);
	}
}
