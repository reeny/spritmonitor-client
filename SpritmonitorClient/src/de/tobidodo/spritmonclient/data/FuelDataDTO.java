/*
 * Copyright (C) 2011 Reeny
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tobidodo.spritmonclient.data;

import de.tobidodo.spritmonclient.util.LogHelper;

/**
 * Data container for fuel data.
 * @author user
 */
public class FuelDataDTO {
	private Integer currentDbId;
	private String totalDistanceValue;
	private String periodDistanceValue;
	private String fuelAmountValue;
	private String fuelPriceValue;
	private Integer lastErrorId;
	private String updatedTime;
	private String transmittedTime;
	
	
	public FuelDataDTO(final String[] attributes){
		setCurrentDbId(attributes[0]);
		setTotalDistanceValue(attributes[1]);
		setPeriodDistanceValue(attributes[2]);
		setFuelAmountValue(attributes[3]);
		setFuelPriceValue(attributes[4]);
		setLastErrorId(attributes[5]);
		setUpdatedTime(attributes[6]);
		setTransmittedTime(attributes[7]);
	}
	
	public FuelDataDTO(){
		
	}
	


	public Integer getCurrentDbId() {
		return currentDbId;
	}


	public void setCurrentDbId(final Integer currentDbId) {
		this.currentDbId = currentDbId;
	}
	

	public void setCurrentDbId(final String idValue) {
		if (idValue == null || idValue.length() == 0){
			this.currentDbId = null;
		}
		else{
			try{
				this.currentDbId = Integer.valueOf(idValue);
			}
			catch(NumberFormatException nfex){
				LogHelper.logError("Error while parsing an id as Long.", nfex);
				this.currentDbId = null;
			}
		}
	}


	public String getTotalDistanceValue() {
		return totalDistanceValue;
	}


	public void setTotalDistanceValue(final String totalDistanceValue) {
		this.totalDistanceValue = totalDistanceValue;
	}


	public String getPeriodDistanceValue() {
		return periodDistanceValue;
	}


	public void setPeriodDistanceValue(final String periodDistanceValue) {
		this.periodDistanceValue = periodDistanceValue;
	}


	public String getFuelAmountValue() {
		return fuelAmountValue;
	}


	public void setFuelAmountValue(final String fuelAmountValue) {
		this.fuelAmountValue = fuelAmountValue;
	}


	public String getFuelPriceValue() {
		return fuelPriceValue;
	}


	public void setFuelPriceValue(final String fuelPriceValue) {
		this.fuelPriceValue = fuelPriceValue;
	}
	
	
	public Integer getLastErrorId() {
		return lastErrorId;
	}

	public void setLastErrorId(final Integer msgId) {
		this.lastErrorId = msgId;
	}

	public void setLastErrorId(final String msgId) {
		this.lastErrorId = msgId != null ? Integer.valueOf(msgId) : null;
	}
	

	public String getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(final String updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getTransmittedTime() {
		return transmittedTime;
	}

	public void setTransmittedTime(final String transmittedTime) {
		this.transmittedTime = transmittedTime;
	}

	public boolean isEmpty(){
		Integer id = getCurrentDbId();
		if (id != null && id.longValue() != -1){
			return false;
		}
		
		String curValue = getTotalDistanceValue();
		if (curValue != null && curValue.length() > 0){
			return false;
		}
		
		curValue = getPeriodDistanceValue();
		if (curValue != null && curValue.length() > 0){
			return false;
		}
		
		curValue = getFuelAmountValue();
		if (curValue != null && curValue.length() > 0){
			return false;
		}
		
		curValue = getFuelPriceValue();
		if (curValue != null && curValue.length() > 0){
			return false;
		}
		
		return true;
	}
}
