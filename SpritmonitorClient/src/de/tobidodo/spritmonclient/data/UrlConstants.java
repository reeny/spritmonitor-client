/*
 * Copyright (C) 2011 Reeny
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tobidodo.spritmonclient.data;



public enum UrlConstants {
	
	LOGIN_URL("http", "www.spritmonitor.de", "/de/anmelden.html"),
	
	ADD_FUEL_URL_DE("http", "www.spritmonitor.de", "/de/betankungen/%d/1/hinzufuegen.html"),
	ADD_FUEL_URL_FR("http", "www.spritmonitor.de", "/fr/pleins/%d/1/ajouter.html"),
	ADD_FUEL_URL_EN("http", "www.spritmonitor.de", "/en/fuelings/%d/1/add.html");
	
	private final String protocol;
	private final String host;
	private final String subpath;
	
	private UrlConstants(String protocol, String host, String subpath){
		this.protocol = protocol;
		this.host = host;
		this.subpath = subpath;
	}

	public String protocol() {
		return protocol;
	}

	public String host() {
		return host;
	}

	public String subpath() {
		return subpath;
	}
	
	public static UrlConstants getUrlConstantsForAddFuel(UserLanguage lang){
		if (lang == null){
			throw new NullPointerException("no user language given (null) - URL cannot be determined");
		}
		
		String urlEnumName = "ADD_FUEL_URL_"+lang.name().toUpperCase();
		return valueOf(urlEnumName);
	}
}
