/*
 * Copyright (C) 2011 Reeny
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tobidodo.spritmonclient.data;

import de.tobidodo.spritmonclient.util.RequestParamProvider;

public class RequestParamDTO implements RequestParamProvider {

	private String loginUserName;
	private String loginUserPassword;
	private String totalDistanceValue;
	private String periodDistanceFormValue;
	private String fuelAmountFormValue;
	private String fuelPriceFormValue;
	private String fuelType;
	
	
	@Override
	public String getLoginUserName() {
		return loginUserName;
	}
	public void setLoginUserName(String loginUserName) {
		this.loginUserName = loginUserName;
	}
	@Override
	public String getLoginUserPassword() {
		return loginUserPassword;
	}
	public void setLoginUserPassword(String loginUserPassword) {
		this.loginUserPassword = loginUserPassword;
	}
	@Override
	public String getTotalDistanceValue() {
		return totalDistanceValue;
	}
	public void setTotalDistanceValue(String totalDistanceValue) {
		this.totalDistanceValue = totalDistanceValue;
	}
	@Override
	public String getPeriodDistanceFormValue() {
		return periodDistanceFormValue;
	}
	public void setPeriodDistanceFormValue(String periodDistanceFormValue) {
		this.periodDistanceFormValue = periodDistanceFormValue;
	}
	@Override
	public String getFuelAmountFormValue() {
		return fuelAmountFormValue;
	}
	public void setFuelAmountFormValue(String fuelAmountFormValue) {
		this.fuelAmountFormValue = fuelAmountFormValue;
	}
	@Override
	public String getFuelPriceFormValue() {
		return fuelPriceFormValue;
	}
	public void setFuelPriceFormValue(String fuelPriceFormValue) {
		this.fuelPriceFormValue = fuelPriceFormValue;
	}
	@Override
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	
	
}
